"""
[141, 1, 17, -7, -17, -27, 18, 541, 8, 7, 7]

[18,141,541]

"""


class Solution():
    def findThreeLargestNumbers(self, nums):
        res = [None, None, None]
        for num in nums:
            self.updatelargest(res, num)
        return res

    def updatelargest(self, res, num):
        if res[2] == None or num > res[2]:
            self.shiftandupdate(res, num, 2)
        elif res[1] == None or num > res[1]:
            self.shiftandupdate(res, num, 1)
        elif res[0] == None or num > res[0]:
            self.shiftandupdate(res, num, 0)

    def shiftandupdate(self, res, num, idx):
        for i in range(idx + 1):
            if i == idx:
                res[i] = num
            else:
                res[i] = res[i + 1]

        """
        if len(nums) == 0: return 0
        if len(nums) == 3: return nums
        i = 0
        self.findThreeLargestNumbersHelper(nums, i)


    def findThreeLargestNumbersHelper(self, nums, i):
        x = nums.pop()
        while i != len(nums):
            if nums[i] < x:
                i += 1
            elif nums[i] > x:
                i = 0
                x=0
                self.findThreeLargestNumbersHelper(nums, i)
            else:
                res=[]
                res.append(x)
        return res
"""


if __name__ == "__main__":
    s = Solution()
    print(s.findThreeLargestNumbers([1, 2, 3, 4, 5, 6, 7, 8, 7]))
