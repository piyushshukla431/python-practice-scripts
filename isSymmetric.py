"""
Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3



"""

class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        if root is None:
            return True
        if root.left.val == root.right.val:
            return True
        if root.left.left.val == root.right.right.val:
            return True
        if root.left.right.val == root.right.left.val:
            return True
        if root.right.val is None:
            return False
        if root.left.val is None:
            return False
        else:
            return False