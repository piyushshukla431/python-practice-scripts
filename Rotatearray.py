class Solution:
    def reverse(self, nums, k):
        res = [0] * len(nums)
        for i in range(len(nums)):
            if i + k < len(nums):
                res[i + k] = nums[i]
            else:
                new_index = (i + k) % len(nums)
                res[new_index] = nums[i]
        return res


if __name__ == "__main__":
    s = Solution()
    print(s.reverse([1, 2, 3, 4, 5, 6, 7], 3))
