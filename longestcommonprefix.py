"""
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".



Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"

"""


class Solution():
    def longestCommonPrefix(self, strs):
        if len(strs) == "": return ""
        ls = len(strs)
        count = 0
        temp = ""
        if ls == 0:
            return ""
        for i in range(ls - 1):
            if (strs[i][:2] == strs[i + 1][:2]):
                count += 1
                temp = strs[i][:2]
                #return temp

        return temp


if __name__ == "__main__":
    s = Solution()
    print(s.longestCommonPrefix(["apple","banana","baba","ape","bat","ball","cat","car"]))
