"""
Given an integer array nums.
find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
"""


class Solution():
    def kadane(self, nums):
        maxsofar = maxendinghere = nums[0]
        for num in nums[1:]:
            maxendinghere = max(num, maxendinghere + num)
            maxsofar = max(maxendinghere, maxsofar)
        return maxsofar


if __name__ == "__main__":
    s = Solution()
    print(s.kadane([-2, 1, -3, 4, -1, 2, 1, -5, 4]))
