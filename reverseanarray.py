"""
Write a program to reverse an array or string

Last Updated: 08-09-2020

Given an array (or string), the task is to reverse the array/string.
Examples :


Input  : arr[] = {1, 2, 3}
Output : arr[] = {3, 2, 1}

Input :  arr[] = {4, 5, 1, 2}
Output : arr[] = {2, 1, 5, 4}
"""
class Solution:
    def reverse(self,array):
        start=0
        end=len(array)-1
        while start < end:
            array[start],array[end]=array[end],array[start]
            start+=1
            end-=1
        return array
if __name__=="__main__":
    s=Solution()
    print(s.reverse([1,2,3,4]))
