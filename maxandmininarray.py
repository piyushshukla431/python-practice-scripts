"""
Maximum and minimum of an array using minimum number of comparisons


"""
class Solution():
    def maxandmin(self, array):
        sorted=False
        ls=len(array)-1
        while sorted is False:
            sorted=True
            for i in range(0,ls):
                if array[i]>array[i+1]:
                    array[i],array[i+1]=array[i+1],array[i]
                    sorted=False
        return array[0],array[ls]


if __name__=="__main__":
    s=Solution()
    print(s.maxandmin([1,2,3,4,55,100,45,2,6,7]))


