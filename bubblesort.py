class Solution():
    def bubbleSort(self, nums):
        # Write your code here.
        sorted = False
        sorted_index = len(nums) - 1
        while sorted is False:
            sorted = True
            for i in range(0, sorted_index):
                if nums[i] > nums[i + 1]:
                    nums[i], nums[i + 1] = nums[i + 1], nums[i]
                    sorted = False
        return nums


if __name__ == "__main__":
    s = Solution()
    print(s.bubbleSort([8, 5, 2, 9, 5, 6, 3]))
