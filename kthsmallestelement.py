"""
Kth smallest element
"""
class SOlution():
    def ksmallelementsol1(self,nums,k):
        sorted=False
        ls=len(nums)-1
        while sorted is False:
            sorted = True
            for i in range(0,ls):
                if nums[i]>nums[i+1]:
                    nums[i],nums[i+1]=nums[i+1],nums[i]
                    sorted=False
        return nums[k+1]
    def ksmallelementsol2(self,nums,k):
        return sorted(nums)[k+1]
if __name__=="__main__":
    s=SOlution()
    print(s.ksmallelementsol2([2,6,8,10,4,3,20,15],3))