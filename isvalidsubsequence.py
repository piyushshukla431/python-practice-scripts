"""
"array": [5, 1, 22, 25, 6, -1, 8, 10],
"sequence": [1, 6, -1, 10]
Output = True



"""


class Solution:
    def isValidSubsequence(x1, sequence):
        xindex = 0
        seq_index = 0
        while xindex < len(x1) and seq_index < len(sequence):
            if x1[xindex] == sequence[seq_index]:
                seq_index += 1
            xindex += 1
        return seq_index == len(sequence)


if __name__ == "__main__":
    s = Solution()
    x=[5, 1, 22, 25, 6, -1, 8, 10]
    y=[1, 6, -1, 10]
    z= s.isValidSubsequence(x,y)
    print(z)
