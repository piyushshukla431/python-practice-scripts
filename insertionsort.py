class Solution():
    def insertionsort(self, array):
        for i in range(1, len(array)):
            j = i
            while j > 0 and array[j] < array[j - 1]:
                self.swap(j, j-1, array)
                j -= 1
        return array

    def swap(self, j, i, array):
        array[i], array[j] = array[j], array[i]

if __name__ == "__main__":
    s=Solution()
    print(s.insertionsort([8, 5, 2, 9, 5, 6, 3]))