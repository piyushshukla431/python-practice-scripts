"""
Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.


Example 1:

Input: s = "()"
Output: true

"""

class Solution():
    def isValid(self,s):
        stack=[]
        for i in range(len(s)):
            if self.isOpenParam(s[i]):
                stack.append(s[i])
            else:
                if len(stack) == 0:
                    return False
                op = stack.pop()
                cl = s[i]
                isvalid = self.isValidParam(op, cl)
                if not isvalid:
                    return False
        return len(stack) == 0

    def isOpenParam(self,p):
        if p=="(" or p =="{" or p=="[":
            return True
        return False
    def isValidParam(self,op,cl):
        if op=="(" and cl==")":
            return True
        if op=="[" and cl=="]":
            return True
        if op=="{" and cl=="}":
            return True
        else: return False

if __name__ == '__main__':
    s = Solution()
    print(s.isValid("[]"))

