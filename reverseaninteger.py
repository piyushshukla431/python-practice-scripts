"""
Given a 32-bit signed integer, reverse digits of an integer.

Note:
Assume we are dealing with an environment that could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

Example 1:

Input: x = 123
Output: 321
"""


class Solution():
    def reverse(self, x):
        def divide(num, divider):
            return int((num * 1.0) / divider)

        def mod(num, m):
            if num < 0:
                return num % -m
            else:
                return num % m

        res = 0
        max_num = pow(2, 31) - 1
        min_num = pow(-2, 31)
        if x < min_num or x > max_num:
            return 0
        else:
            while x:
                y = mod(x, 10)
                x = divide(x, 10)
                res = res * 10 + y
                if res < min_num or res > max_num: return 0
            return res


if __name__ == '__main__':
    s = Solution()
    print(s.reverse(124571))
