class Solution:
    def isPalin(self,string):
        rev_string=""
        for i in reversed(string):
            rev_string= rev_string+i
        return rev_string == string

if __name__== '__main__':
    s=Solution()
    print(s.isPalin("abcddcbaq"))