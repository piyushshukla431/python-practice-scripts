class Solution:
    def removeDuplicates(self, nums):
        dup_count=0
        for i in range(1,len(nums)):
            if nums[i]==nums[i-1]:
                dup_count+=1
            else:
                nums[i-dup_count]=nums[i]
        return len(nums)-dup_count

if __name__=='__main__':
    s=Solution()
    print(s.removeDuplicates([1,1,2]))


"""
class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        dup_cnt = 0                                     # Keep count of the duplicates
        for i in range(1, len(nums)):
            if nums[i] == nums[i-1]:                # Found a duplicate
                dup_cnt += 1                           # Increase duplicate count by 1
            else:
                nums[i - dup_cnt] = nums[i]     # Copy the value to a previous index w.r.t number of duplicates
        return(len(nums) - dup_cnt)

"""