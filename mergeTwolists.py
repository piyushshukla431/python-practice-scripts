"""
#########INCOMPLETE#####################
Merge two sorted linked lists and return it as a new sorted list.
The new list should be made by splicing together the nodes of the first two lists.

Input: l1 = [1,2,4], l2 = [1,3,4]
Output: [1,1,2,3,4,4]

The number of nodes in both lists is in the range [0, 50].
-100 <= Node.val <= 100
Both l1 and l2 are sorted in non-decreasing order.
"""
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

class Solution:
    def mergeTwoLists(self, l1, l2):

# Create a Dummy node and initialize it with 0
        prev = ListNode(0)
        root = prev

# While either lists are not empty we append the lesser to our new list l3
        while (l1 != None) and (l2 != None):
            if l1.val <= l2.val:
                l3 = ListNode(l1.val)
                prev.next = l3
                prev = l3
                l1 = l1.next
            else:
                l3 = ListNode(l2.val)
                prev.next = l3
                prev = l3
                l2 = l2.next

# Append the non empty list to l3
        if l1 != None:
            prev.next = l1

        if l2 != None:
            prev.next = l2

# Return the starting of list l3 (Dummy node.next)
        return root.next
if __name__ == "__main__":
    s = Solution()
    print(s.mergeTwoLists([1, 2, 3, 4], [2, 3, 6, 7]))
