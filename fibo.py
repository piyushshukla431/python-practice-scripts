class Solution():
    def getNthFib(self, n):
        if n==1:return 0
        if n==2:return 1

        res=[0,1]
        count=3
        while count<=n:
            nexnum=res[0]+res[1]
            res[0]=res[1]
            res[1]=nexnum
            count+=1
        return res[1]







if __name__ == "__main__":
    s = Solution()
    y = s.getNthFib(6)
    print(y)
