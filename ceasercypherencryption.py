"""
ceasercypherencryption

{"string": "xyz", "key": 2}
o/p: zab

"""


class Solution():
    def caesarCipherEncryptor(self, string, key):
        newletters = []
        newkey = key % 26
        for char in string:
            newletters.append(self.getnewletter(char, newkey))
        return "".join(newletters)

    def getnewletter(self, letter, key):
        newlettercode = ord(letter) + key
        if newlettercode <= 122:
            return chr(newlettercode)
        else:
            return chr(96 + newlettercode%122)
if __name__=="__main__":
    s=Solution()
    print(s.caesarCipherEncryptor("xyz",2))
