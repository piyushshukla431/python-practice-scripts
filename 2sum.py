"""
array=[3, 5, -4, 8, 11, 1, -1, 6]
targetSum=10
Output=[11,-1]
"""


class Solution:
    def twoSum(self, array, targetSum):
        for i in range(len(array)-1):
            for j in range(i+1,len(array)):
                #if array[i]+array[j]==targetSum and array[i]!=array[j]:
                if array[i] + array[j] == targetSum:
                    return [array[i],array[j]]
        return []
    """
    Solution 1
        result = {}
        for num in array:
            x = targetSum - num
            # if x in array and x!=num:
            if x in result:
                return [num, x]
            else:
                result[num] = True
        return []

"""
if __name__ == "__main__":
    s = Solution()
    print(s.twoSum([3, 5, -4, 8, 11, 1, -1, 6], 10))
