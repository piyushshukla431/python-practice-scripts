class Solution():
    def intersection(self,nums1,nums2):
        res=[]
        if len(nums2)==0 or len(nums1) == 0:
            return res
        ls=len(nums2)
        for i in range (0,ls):
            if nums2[i] in nums1:
                res.append(nums2[i])
        return set(res)


if __name__=="__main__":
    s=Solution()
    print(s.intersection([1],[1]))