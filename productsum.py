"""
{"array": [5, 2, [7, -1], 3, [6, [-13, 8], 4]]}
output: 12
"""


def productSum(array=None, multiplier=1):
    if array is None:
        array = [5, 2, [7, -1], 3, [6, [-13, 8], 4]]
    sum = 0
    for element in array:
        if type(element) is list:
            sum += productSum(element, multiplier + 1)
        else:
            sum += element
    return sum * multiplier
