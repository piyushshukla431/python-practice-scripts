import socket
import subprocess
import sys
from datetime import datetime

# sock = socket.socket(socketfamily, sockettype) ====> syntax for creating a socket
# sock - socket.socket(socket.AF_INET6,socket.SOCK_STREAM)
# print(socket.gethostbyname_ex("osomstuff.com"))
# print(socket.gethostname())
# print(socket.getfqdn("8.8.8.8"))

subprocess.call("clear", shell=True)
remote_server = input("enter the hostname")
remote_server_ip = socket.gethostbyname(remote_server)
print("#$%" * 30)
print("scanning host . Please wait")
print("#$%" * 30)
time1 = datetime.now()
try:
    for portrange in (1, 10):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((remote_server_ip, portrange))
        if result == 0:
            print("port open ".format(portrange))
            sock.close()
except KeyboardInterrupt:
    print("you print control c")
    sys.exit()
except socket.gaierror:
    print("host could not be resolved")
except socket.error:
    print("could not connect to host")
time2 = datetime.now()
time = time2 - time1
print("scanning completed in " + time)
